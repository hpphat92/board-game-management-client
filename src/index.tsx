import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './app/App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import {ApiService} from './app/common/services/api.service';
import {ErrorInterceptor} from './app/common/interceptors/error.interceptor';

ReactDOM.render(<App/>, document.getElementById('root'));


ApiService.addInterceptor(new ErrorInterceptor());


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
