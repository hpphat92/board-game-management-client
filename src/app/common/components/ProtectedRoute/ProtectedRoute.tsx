import React, {FunctionComponent} from 'react';
import {Redirect, Route, RouteProps} from 'react-router';
import {RouteComponentProps} from '@reach/router';

type Props = { component: FunctionComponent } & RouteComponentProps | any;

const ProtectedRoute: FunctionComponent<Props> = ({component: Component, ...rest}) => {

    function isAuth(): any {
        return !!localStorage.getItem('accessToken');
    }

    function redirectTo(path: string, props: any) {
        return <Redirect to={{
            pathname: `/${path}`,
            state: {from: props.location}
        }}/>;
    }

    //rest.auth & isAuth()  then
    // 1            1      => Go normally
    // 0            1      => Home
    // 1            0      => Login
    // 0            0      => Go normally
    return (
        <Route {...rest} render={(props: any) => {
            if (rest.auth ^ isAuth()) {
                if (rest.auth) {
                    return redirectTo('login', props);
                }
                return redirectTo('home', props);
            } else {
                return <Component {...props} />;
            }
        }}/>
    );

};

export default ProtectedRoute;
