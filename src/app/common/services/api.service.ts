import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';
import config from '../../../../src/config.json';
import {IInterceptor} from '../model/Interceptor';

const _axios: AxiosInstance = axios.create({
    baseURL: config.BASE_URL,
    responseType: 'json'
});

export class ApiService {

    constructor() {
    }


    static addInterceptor(interceptor: IInterceptor){
        _axios.interceptors.response.use(interceptor.onFulfilled, interceptor.onRejected);
    }

    static request<T = any, R = AxiosResponse<T>>(config: AxiosRequestConfig): Promise<R> {
        return _axios.request(config);
    };

    static get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return _axios.get(url, config);
    };

    static delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return _axios.delete(url, config);
    };

    static head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return _axios.head(url, config);
    };

    static post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
        return _axios.post(url, data, config);
    };

    static put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
        return _axios.put(url, data, config);
    };

    static patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
        return _axios.patch(url, data, config);
    };
}
