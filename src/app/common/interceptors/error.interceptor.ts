import {ToastsStore} from 'react-toasts';
import {IInterceptor} from '../model/Interceptor';

export class ErrorInterceptor implements IInterceptor {

    public onFulfilled(value: any): Promise<any> | any {
        return Promise.resolve(value);
    }

    public onRejected(err: any): any {
        ToastsStore.error(err.response.data.message);
        return Promise.reject(err.response.data);
    }

}
