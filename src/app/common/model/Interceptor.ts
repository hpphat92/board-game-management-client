export interface IInterceptor {

    onFulfilled(value: any): Promise<any> | any;

    onRejected(err: any): any;
}
