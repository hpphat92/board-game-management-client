import React from 'react';
import * as PropTypes from 'prop-types';
import './Register.scss';
import axios from 'axios';
import {Formik, FormikActions} from 'formik';
// @ts-ignore
import {ApiService} from '../../common/services/api.service';
import {ToastsStore} from 'react-toasts';

const Register: React.FC = (props: any) => {

    function goBackToLogin() {
        props.history.push('/login');
    }

    function onSubmit(values: any, formikActions: FormikActions<any>) {
        const model = {
            ...values,
            callback: `${window.location.host}//${window.location.host}/validate`
        };
        ApiService.post('/register', values)
            .then((...args) => {
                ToastsStore.success('check_mail');
                props.history.push('/login');
            }, () => {
            });
    }

    function validate(values: any) {
        let errors: any = {};
        if (!values.email) {
            errors.email = 'Email is required';
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address';
        }
        if (!values.password) {
            errors.password = 'Password is required';
        }
        if (!values.confirmPassword) {
            errors.confirmPassword = 'Confirm password is required';
        }
        if (values.password !== values.confirmPassword && values.confirmPassword) {
            errors.confirmPassword = 'Confirm password not match';
        }
        return errors;
    }

    return (
        <div className="register-container">
            <div className="form-container">
                <div className="header"><b>Board game</b> <em>with us</em></div>
                <div className="sub-header"></div>

                <div className="instruction">Please login to enjoy our fantastic world</div>
                <Formik onSubmit={onSubmit} initialValues={
                    {
                        firstName: '',
                        lastName: '',
                        email: '',
                        password: '',
                        confirmPassword: ''
                    }
                }
                        validate={validate}>
                    {
                        ({
                             values,
                             errors,
                             touched,
                             handleChange,
                             handleBlur,
                             handleSubmit,
                             isSubmitting,
                             dirty,
                             isValid
                         }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="firstName">First name</label>
                                    <input type="text" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="firstName"
                                           value={values.firstName}/>
                                    {errors.firstName && touched.firstName && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.firstName}</small>)}

                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastName">Last name</label>
                                    <input type="text" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="lastName"
                                           value={values.lastName}/>
                                    {errors.lastName && touched.lastName && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.lastName}</small>)}

                                </div>
                                <div className="form-group">
                                    <label htmlFor="email" className="required">Email</label>
                                    <input type="email" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="email"
                                           value={values.email}/>
                                    {errors.email && touched.email && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.email}</small>)}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password" className="required">Password</label>
                                    <input type="password" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="password"
                                           value={values.password}/>
                                    {errors.password && touched.password && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.password}</small>)}
                                </div>
                                <div className="form-group">
                                    <label htmlFor="confirmPassword" className="required">Confirm password</label>
                                    <input type="password" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="confirmPassword"
                                           value={values.confirmPassword}/>
                                    {errors.confirmPassword && touched.confirmPassword && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.confirmPassword}</small>)}
                                </div>
                                <div className="button-footer">
                                    <button type="submit" className="btn-login btn btn-primary"
                                            disabled={!(dirty && isValid)}>Register
                                    </button>
                                    <button className="btn btn-link new-user" type="button" onClick={goBackToLogin}>
                                        Already have account. Login now!
                                    </button>
                                </div>
                            </form>
                        )
                    }
                </Formik>
            </div>
        </div>
    );
};
Register.propTypes = {
    history: PropTypes.object.isRequired
};
export default Register;
