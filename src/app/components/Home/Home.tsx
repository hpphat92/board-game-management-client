import React from 'react';
import './Home.css';
import {RouteComponentProps} from '@reach/router';

const Home = (props: any) => {

    function signOut() {
        localStorage.removeItem('accessToken');
        props.history.push('/login');
    }

    return (
        <>
            <div>
                Hello this is home page
            </div>
            <button onClick={signOut}>Signout</button>
        </>
    );
};

export default Home;
