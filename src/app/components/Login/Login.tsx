import React from 'react';
import './Login.scss';
import {Formik, FormikActions} from 'formik';
// @ts-ignore
import {ApiService} from '../../common/services/api.service';
import {Link} from 'react-router-dom';

const Login: React.FC = (props: any) => {


    function onSubmit(values: any, formikActions: FormikActions<any>) {
        ApiService.post('/signin', values)
            .then(({data}) => {
                const {user, accessToken} = data;
                localStorage.setItem('accessToken', accessToken);
                props.history.push('/home');
            }, () => {
                debugger;
            });
    }

    function validate(values: any) {
        let errors: any = {};
        if (!values.email) {
            errors.email = 'Email is required';
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address';
        }
        if (!values.password) {
            errors.password = 'Password is required';
        }
        return errors;
    }

    return (
        <div className="Login-container">
            <div className="form-container">
                <div className="header"><b>Board game</b> <em>with us</em></div>
                <div className="sub-header"></div>

                <div className="instruction">Please login to enjoy our fantastic world</div>
                <Formik onSubmit={onSubmit} initialValues={
                    {
                        email: '',
                        password: ''
                    }
                }
                        validate={validate}>
                    {
                        ({
                             values,
                             errors,
                             touched,
                             handleChange,
                             handleBlur,
                             handleSubmit,
                             isSubmitting,
                             dirty,
                             isValid
                         }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input type="email" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="email"
                                           value={values.email}/>
                                    {errors.email && touched.email && (
                                        <small id="emailHelp" className="form-text text-danger">{errors.email}</small>)}

                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input type="password" className="form-control" onChange={handleChange}
                                           onBlur={handleBlur}
                                           id="password"
                                           value={values.password}/>
                                    {errors.password && touched.password && (
                                        <small id="emailHelp"
                                               className="form-text text-danger">{errors.password}</small>)}
                                </div>
                                <div className="button-footer">
                                    <button type="submit" className="btn-login btn btn-primary"
                                            disabled={!(dirty && isValid)}>Sign in!
                                    </button>
                                    <Link to="/register" className="btn btn-link new-user">I am new user</Link>
                                </div>
                            </form>
                        )
                    }
                </Formik>
            </div>
        </div>
    );
};

export default Login;
