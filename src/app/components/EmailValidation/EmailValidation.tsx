import React from 'react';
import {ToastsStore} from 'react-toasts';
import {ApiService} from './../../common/services/api.service';

const EmailValidation: React.FC = (props: any) => {
    const token = new URLSearchParams(props.location.search).get('token');
    if (!token) {
        ToastsStore.error('invalid_url');
        props.history.push('/login');
    } else {
        ApiService.get(`api/verify?token=${token}`)
            .then(() => {
                ToastsStore.success('success_activated');
                props.history.push('/login');
            }, () => {
            });
    }
    return (
        <div></div>
    );
};

export default EmailValidation;
