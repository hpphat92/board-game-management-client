import React from 'react';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import './App.scss';
import Login from './components/Login/Login';
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from 'react-toasts';
import Register from './components/Register/Register';
import Home from './components/Home/Home';
import EmailValidation from './components/EmailValidation/EmailValidation';
import ProtectedRoute from './common/components/ProtectedRoute/ProtectedRoute';


const App: React.FC = () => {
    return (
        <>
            <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.BOTTOM_RIGHT}/>
            <Router>
                <Route exact path="/" render={() => {
                    return <Redirect to="/login"/>;
                }}/>
                <ProtectedRoute path="/login" component={Login}/>
                <ProtectedRoute path="/register" component={Register}/>
                <ProtectedRoute path="/validate" component={EmailValidation}/>
                <ProtectedRoute auth={true} path="/home" component={Home}>
                </ProtectedRoute>
            </Router>
        </>
    );
};

export default App;
